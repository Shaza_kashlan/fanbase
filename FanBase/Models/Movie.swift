//
//  Movie.swift
//  FanBase
//
//  Created by shaza kashlan on 17/02/2021.
//

import Foundation


struct Movie {
    
    private(set) var name: String
    private(set) var actorArray: [Actor]
    
    init(name: String, actors:[Actor]) {
        self.name = name
        self.actorArray = actors
    }
}
