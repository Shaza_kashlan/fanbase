//
//  Actor.swift
//  FanBase
//
//  Created by shaza kashlan on 17/02/2021.
//

import Foundation


struct Actor {
    
    private(set) var name : String
    private(set) var image : String
    private(set) var desription: String
    
    init(name: String, image: String, desription: String) {
        
        self.name = name
        self .image = image
        self.desription = desription
    }
    
}
