//
//  ActorCollectionViewCell.swift
//  FanBase
//
//  Created by shaza kashlan on 18/02/2021.
//

import UIKit

class ActorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var actorImage: UIImageView!
    @IBOutlet weak var actorName: UILabel!
    @IBOutlet weak var actorDescription: UILabel!
        
    var actor : Actor?
    func updateView(actor:Actor){
        self.actor = actor
        actorImage.image = UIImage(named: actor.image)
        actorName.text = actor.name
        actorDescription.text = actor.desription
    }
    
}
