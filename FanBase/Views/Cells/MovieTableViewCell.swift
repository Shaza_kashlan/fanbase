//
//  MovieTableViewCell.swift
//  FanBase
//
//  Created by shaza kashlan on 17/02/2021.
//

import UIKit

protocol CollectionViewCellDelegate: class {
    func collectionView(collectionviewcell: ActorCollectionViewCell?, index: Int, didTappedInTableViewCell: MovieTableViewCell)
}

class MovieTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource  {
   
    weak var cellDelegate: CollectionViewCellDelegate?
    @IBOutlet weak var actorsCollectionView:
        UICollectionView!
    
    private(set) var actors:[Actor]=[]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        actorsCollectionView.dataSource = self
        actorsCollectionView.delegate = self
        let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            flowLayout.itemSize = CGSize(width: 180, height: 180)
            flowLayout.minimumLineSpacing = 2.0
            flowLayout.minimumInteritemSpacing = 4.0
            self.actorsCollectionView.collectionViewLayout = flowLayout
            self.actorsCollectionView.showsHorizontalScrollIndicator = false
    }
    
    func initActors(movieName:String) {
        actors = DataService.instance.getActors(movieName: movieName)
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actors.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let actorCell = actorsCollectionView.dequeueReusableCell(withReuseIdentifier: "actorCell", for: indexPath) as? ActorCollectionViewCell{
            let actor = actors[indexPath.row]
            actorCell.updateView(actor: actor)
            return actorCell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ActorCollectionViewCell
         self.cellDelegate?.collectionView(collectionviewcell: cell, index: indexPath.item, didTappedInTableViewCell: self)
    }
 
    
}



