//
//  DataServices.swift
//  FanBase
//
//  Created by shaza kashlan on 17/02/2021.
//

import Foundation

class DataService {
    
    static let instance = DataService()
    
    var movies : [String : [Actor]] = ["Movie1": [Actor(name: "actor1", image: "hat01", desription:"hat1"),
                                                  Actor(name: "actor2", image: "hat02", desription: "hat2"),
                                                  Actor(name: "actor3", image: "hat03", desription: "hat3")],
                                       "Movie2": [Actor(name:"actor1",image:"hoodie01",desription:"hoodie1"),
                                                  Actor(name:"actor2",image:"hoodie02",desription:"hoodie2"),
                                                  Actor(name:"actor3",image:"hoodie03",desription:"hoodie3")],
                                       "Movie3": [Actor(name:"actor1",image:"shirt01",desription:"shirt1"),
                                                  Actor(name:"actor2",image:"shirt02",desription:"shirt2"),
                                                  Actor(name:"actor3",image:"shirt03",desription:"shirt3")]]
    
    func getMovie(index:Int) -> String {
        let Key = Array(movies.keys.sorted())[index]
        return Key
    }
    
    func getMovies() -> [String] {
        return Array(movies.keys)
    }
    
    func getActors(movieName: String) ->[Actor] {
        
        return movies[movieName]!
    }
}
