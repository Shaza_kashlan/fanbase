//
//  actorCollectionViewCellDelegate.swift
//  FanBase
//
//  Created by shaza kashlan on 19/02/2021.
//

import Foundation

protocol CollectionViewCellDelegate: class {
    func collectionView(collectionviewcell: CollectionViewCell?, index: Int, didTappedInTableViewCell: TableViewCell)
    // other delegate methods that you can define to perform action in viewcontroller
}
