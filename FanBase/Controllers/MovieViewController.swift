//
//  MovieViewController.swift
//  FanBase
//
//  Created by shaza kashlan on 18/02/2021.
//

import UIKit

class MovieViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,CollectionViewCellDelegate  {
  
    @IBOutlet weak var movieTableView: UITableView!
    var tappedCell: ActorCollectionViewCell!

    override func viewDidLoad() {
        super.viewDidLoad()

        movieTableView.dataSource = self
        movieTableView.delegate = self
        // Enable automatic row auto layout calculations
        self.movieTableView.rowHeight = UITableView.automaticDimension;
        // Set the estimatedRowHeight to a non-0 value to enable auto layout.
        self.movieTableView.estimatedRowHeight = 10;
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return DataService.instance.getMovies().count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            let titleLabel = UILabel(frame: CGRect(x: 8, y: 0, width: 200, height: 44))
            headerView.addSubview(titleLabel)
            titleLabel.textColor = UIColor.white
            titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            titleLabel.text =  DataService.instance.getMovie(index: section)
            return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 44
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = movieTableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as? MovieTableViewCell {
            print("section:\(indexPath.section)")
            print("row:\(indexPath.row)")

            let movieName = DataService.instance.getMovie(index: indexPath.section)
            print(movieName)
            cell.initActors(movieName: DataService.instance.getMovie(index: indexPath.section))
            cell.cellDelegate = self
            return cell
        }
        return MovieTableViewCell()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsVC" {
            let barBtn = UIBarButtonItem()
            barBtn.title = ""
            navigationItem.backBarButtonItem = barBtn
            let DestViewController = segue.destination as! ActorDetailsViewController
            DestViewController.actorNameTxt = tappedCell.actorName.text!
            DestViewController.actorDescriptionTxt = tappedCell.actorDescription.text!
            DestViewController.image = tappedCell.actorImage.image
        }
    }
    
    func collectionView(collectionviewcell: ActorCollectionViewCell?, index: Int, didTappedInTableViewCell: MovieTableViewCell) {
        self.tappedCell = collectionviewcell
        self.performSegue(withIdentifier: "detailsVC", sender: self)
    }
    

}
