//
//  actorDetailsViewController.swift
//  FanBase
//
//  Created by shaza kashlan on 18/02/2021.
//

import UIKit

class ActorDetailsViewController: UIViewController {

    @IBOutlet weak var actorImage: UIImageView!
    
    @IBOutlet weak var actorDescription: UILabel!
    
    var actorNameTxt :String = ""
    var image: UIImage?
    var actorDescriptionTxt :String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = backgroundColor
        self.navigationItem.title = actorNameTxt
        
        self.actorImage.image = image
        
        self.actorDescription.text = actorDescriptionTxt
        // Do any additional setup after loading the view.
    }
}
